from django.db import models

# Create your models here.

class User(models.Model):
	username = models.CharField(max_length=30)
	email = models.CharField(max_length=30)
	password = models.CharField(max_length=30)

class Pits(models.Model):
	username = models.CharField(max_length=30)
	pit = models.TextField()
	date = models.DateTimeField(auto_now_add=True)

class Following(models.Model):
	username = models.CharField(max_length=30)

