from django.urls import path
from django.conf.urls import url
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    #path('', views.index, name='index'),
    #path('login', views.login, name='login'),
    #path('register', views.register, name='register'),
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('friend', views.friend, name='frined')
]

